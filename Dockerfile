# This file is a template, and might need editing before it works on your project.
FROM python:3.6

WORKDIR /app
#install heideltime with special rules
RUN cd $WORKDIR
RUN wget https://www.dropbox.com/s/y33iftyd6wdvb1f/thesis_extras.zip
RUN unzip thesis_extras.zip

#install tree-tagger
RUN mkdir tagger
WORKDIR /app/tagger
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tree-tagger-linux-3.2.2.tar.gz
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/tagger-scripts.tar.gz
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/install-tagger.sh
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/german.par.gz
RUN wget https://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/english.par.gz
RUN chmod +x ./install-tagger.sh
RUN ./install-tagger.sh

#java
RUN apt-get update
RUN apt-get install -y --force-yes software-properties-common
RUN add-apt-repository ppa:openjdk-r/ppa
RUN apt-get install -y --force-yes openjdk-11-jdk

#config for heideltime
RUN rm /app/thesis_extras/heideltime-standalone/config.props
COPY config.props /app/thesis_extras/heideltime-standalone/
COPY heidelWrapper.py /app/thesis_extras/heideltime-standalone/
COPY test.sh /app/thesis_extras/heideltime-standalone/
COPY heidelservice.py /app/thesis_extras/heideltime-standalone/

#flask
RUN pip install flask

#use shiny new models for heideltime
RUN mv /app/thesis_extras/heideltime-standalone/german-new /app/thesis_extras/heideltime-standalone/german

# For Django
EXPOSE 8080
ENTRYPOINT FLASK_APP=/app/thesis_extras/heideltime-standalone/heidelservice.py flask run --host=0.0.0.0 --port=8080
#CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# For some other command
#CMD ["echo", "test"]
