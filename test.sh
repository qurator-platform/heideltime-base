#!/usr/bin/env bash
java -jar /app/thesis_extras/heideltime-standalone/de.unihd.dbs.heideltime.standalone.jar -o TIMEML -e UTF-8 -dct $(date +'%Y-%m-%d') -t NEWS -l german -c /app/thesis_extras/heideltime-standalone/config.props $1
