from flask import Flask, url_for
from flask import request
import heidelWrapper

# Klasse flask initialisieren
app = Flask(__name__)


@app.route("/", methods = ['GET'])
def test():
    query_string = request.args.get('msg')
    wrapper = heidelWrapper.HeidelWrapper(query_string)
    return wrapper.getResponse()


# app auf port 1337 starten (debug=True nur zum Entwickeln später Sicherheitslücke)
if __name__ == '__main__':
    app.run(port = 1337)