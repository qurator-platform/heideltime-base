import subprocess
import tempfile

class HeidelWrapper():
    def __init__(self, input):
        self.input = input

    def getResponse(self):
        fl = tempfile.NamedTemporaryFile(mode="w")
        fl.write(self.input)
        fl.flush()
        rv = subprocess.run(["bash", "/app/thesis_extras/heideltime-standalone/test.sh", fl.name], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        print ("rv=", str(rv))
        return rv.stdout.decode("utf-8")




if __name__ == "__main__":
    wrapper = HeidelWrapper("foobar")
    print (wrapper.getResponse())